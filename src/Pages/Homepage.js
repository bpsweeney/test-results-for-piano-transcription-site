import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './CSS/Home.css';
import './CSS/style.css';
import './CSS/nav.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';

class Homepage extends Component{
    render(){
        return(
            <div>
            <section>
			<Nav />
		    </section>
			<div className='container'>
				<div className = 'row'>
				<div className = 'col'>
				</div>
					<div className = 'col centerT'>
						<div className='nav-link'>
                        <Link to='/SingleNotes'>
							<a href>
                                Simple Note Tests ->
							</a>
                        </Link>
						</div>
					</div>
                </div>
			</div>
            <div className='container'>
                <div className='bar'>
                    <h1 className='h3 marginb3'>
                        This is the piano transcription testing results site
                    </h1>
                </div>
                <div className="negative-box marginb3">
                    This site has wav files tested with given outputs for two different transcription programs.
					<br/>
					If you would rather get a faster rundown of testing results refer to this <a href='https://docs.google.com/spreadsheets/d/1z2Lxq-_AOyLN4rreGj855VmQP4diUmsY3wRMs7J9NBM/edit?usp=sharing'>
					google worksheet </a>.
					<br/>
					It will give you more information about the Simple and Basic tests on this site.
                </div>
                <div className="bar">
                    <h1 className="h5 marginb3">
                        How the programs work
                    </h1>
                </div>
                <div className="negative-box marginb3">
					(Program 1) This program lives in the <a href='https://gitlab.com/bpsweeney/music-transcription/-/tree/main/Backend'>backend</a> folder.  It gets called by app in main.<br/>
                    The processing of the recording is broken down into two different aspects.  
					Firstly, we have to determine where the specific notes are located in the audio file.  
					Secondly, we have to determine which piano note(s) were played.  
					Note location detection: <br/>
					To accomplish this, we took slices of the audio and analyzed the volume levels of the sections to determine where notes took place.  
					This process seems to work very accurately especially when removing all frequencies under a certain value.
					Note detection: <br/>
					With the given audio file, we convert it to an array of frequency magnitudes.  
					We then performed the fast fourier transform on the frequencies to detect the peaks of frequencies.  
					These found peaks in frequencies, once running the fast fourier transform on the given frequencies, is what we used to determine which piano note is being played.  
					By getting the peaks of the frequencies and calculating the multiples of them and how they are related to each other, we were able to predict the existing piano notes. 
					<br/>
					<br/>
					The second program tested is geared to work on songs.  It needs a given bpm and fastest note played to get an idea of how to
					read the input.  It then attempts to produce a sheet that has chords and different paced notes.  To get a better idea of how it works
					check out the code on the git in a folder called Program2.  The folder will have a README that goes into 
					more detail about how it works. This <a href='https://gitlab.com/bpsweeney/music-transcription/-/tree/main/Program2'>
					link  </a>
					will bring you to the folder on git.
                </div>
				 <div className="bar">
                    <h1 className="h5 marginb3">
                        How to get started
                    </h1>
                </div>
				<div className="negative-box marginb3">
                    To check out the results on the site either move around with the navbar at the top or click links on the top of the page.
                </div>
            </div>
			
			<Foot />
            </div>
        )
    }
}

export default Homepage;