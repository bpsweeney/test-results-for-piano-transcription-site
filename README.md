# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

### `npm install`

Running a npm install on the project will give you everything you need to run the site in a node_modules folder as long as you have package and package-lock.\
Once you do that you can use the following command to run the site locally.


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

To stop the localhost hit ctrl-C and type Y.