import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './CSS/Home.css';
import './CSS/style.css';
import './CSS/nav.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';
import C1C8NS from './Assets/originWavs/C1-C8NoSharps.wav';
import C1C8NSP1 from './Assets/SingleNotesPdfs/C1-C8nosharps.pdf';
import C1C8NSP2 from './Assets/SingleNotesPdfs/C1-C8.pdf';

import RLW from './Assets/originWavs/rh_and_lh.wav';
import RLP1 from './Assets/SingleNotesPdfs/Testingnotesintherightandlefthand.pdf';
import RLP2 from './Assets/SingleNotesPdfs/Testingnotesfortherightandlefthand.pdf';

import HS from './Assets/originWavs/heart_and_soul_chords.wav';
import HSP1 from './Assets/SingleNotesPdfs/Heartandsoulchords.pdf';
import HSP2 from './Assets/SingleNotesPdfs/Heartandsoulchord.pdf';

class SingleNoteTests extends Component{
    render(){
		var str = "<- Home";
        return(
            <div>
            <section>
			<Nav />
		    </section>
			<div className='container'>
				<div className = 'row'>
				<div className = 'col centerT'>
					<div className='nav-link'>
                        <Link to='/'>
							<a href>
                                {str}
							</a>
                        </Link>
					</div>
				</div>
					<div className = 'col centerT'>
						<div className='nav-link'>
                        <Link to='/BasicTests'>
							<a href>
                                Basic Note Tests ->
							</a>
                        </Link>
						</div>
					</div>
                </div>
			</div>
			<div className='container set-bg'>
				<div className='text-center h3'>
					Testing left and right hand output.
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={RLW}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={RLP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={RLP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={RLP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={RLP2}>to the PDF!</a></p>
                </object>
            </div>
			
            <div className='container set-bg'>
				<div className='text-center h3'>
					C1-C8 no sharps
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={C1C8NS}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={C1C8NSP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={C1C8NSP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={C1C8NSP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={C1C8NSP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Small piece of Heart and soul with chords
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={HS}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={HSP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HSP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={HSP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HSP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<Foot />
            </div>
        )
    }
}

export default SingleNoteTests;