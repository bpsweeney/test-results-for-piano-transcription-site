import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './CSS/Home.css';
import './CSS/style.css';
import './CSS/nav.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';
import Twi1 from './Assets/originWavs/Twinkle_both_hands.wav';
import Twi1P1 from './Assets/BasicNotesPdfs/TwinkleTwinklelittlestar.pdf';
import Twi1P2 from './Assets/BasicNotesPdfs/TwinkleTwinklelittlestar2.pdf';
import Twi2 from './Assets/originWavs/Twinkle.wav';
import Twi2P1 from './Assets/BasicNotesPdfs/TwinkleTwinkle.pdf';
import Twi2P2 from './Assets/BasicNotesPdfs/TwinkleTwinkle2.pdf';

import YankW from './Assets/originWavs/yankee_doodle.wav';
import YankP1 from './Assets/BasicNotesPdfs/YankeeDoodle.pdf';
import YankP2 from './Assets/BasicNotesPdfs/YankDoodle.pdf';

import JingW from './Assets/originWavs/Jingle_bells.wav';
import JingP1 from './Assets/BasicNotesPdfs/JingleBells.pdf';
import JingP2 from './Assets/BasicNotesPdfs/JingleBells2.pdf';

import MaryW from './Assets/originWavs/marry_had_a_little_lamb.wav';
import MaryP1 from './Assets/BasicNotesPdfs/MaryHadALittleLamb.pdf';
import MaryP2 from './Assets/BasicNotesPdfs/LittleLamb.pdf';

class BasicNoteTests extends Component{
    render(){
		var str = "<- Simple Note Tests";
        return(
            <div>
            <section>
			<Nav />
		    </section>
			
			<div className='container'>
				<div className = 'row'>
				<div className = 'col centerT'>
					<div className='nav-link'>
                        <Link to='/SingleNotes'>
							<a href>
                                {str}
							</a>
                        </Link>
					</div>
				</div>
					<div className = 'col centerT'>
						<div className='nav-link'>
                        <Link to='/NormalTests'>
							<a href>
                                Normal Note Tests ->
							</a>
                        </Link>
						</div>
					</div>
                </div>
			</div>
			
            <div className='container set-bg'>
				<div className='text-center h3'>
					Twinkle Both hands
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={Twi1}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={Twi1P1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Twi1P1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={Twi1P2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Twi1P2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Twinkle low volume.
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={Twi2}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={Twi2P1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Twi2P1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={Twi2P2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Twi2P2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Yankee Doodle
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={YankW}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={YankP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={YankP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={YankP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={YankP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Jingle Bells
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={JingW}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={JingP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={JingP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={JingP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={JingP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Mary had a little lamb
				</div>
				<br />
                {/*<a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.  It's a youtube video.
				</a>*/}
				<br />
				<div className = 'Center-audio'>
				Original wav file
				</div>
				<div className = 'Center-audio'>
					<audio
						id='audio'
						controls
						src={MaryW}
					></audio>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={MaryP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={MaryP1}>to the PDF!</a></p>
                </object>
				<br />
				PDF output for program 2
				<object className="marg-top" title="PDF" data={MaryP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={MaryP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<Foot />
            </div>
        )
    }
}

export default BasicNoteTests;