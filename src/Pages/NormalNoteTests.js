import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './CSS/Home.css';
import './CSS/style.css';
import './CSS/nav.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';

import Hrt from './Assets/originWavs/Titanic.wav';
import HrtP1 from './Assets/NormalNotePdfs/MyHeartWillGoOn.pdf';
import HrtP2 from './Assets/NormalNotePdfs/HeartWillGoOn.pdf';
import Hrtmp31 from './Assets/mp3Normalouts/P1MyHeart.mp3';
import Hrtmp32 from './Assets/mp3Normalouts/P2MyHeartout.mp3';
import HrtP2mscz from './Assets/MsczFiles/MyHeartP2.mscz';
import HrtP1mscz from './Assets/MsczFiles/MyHeartP1.mscz';

import Has from './Assets/originWavs/HeartAndSoul.wav';
import HasP1 from './Assets/NormalNotePdfs/HeartandSoul.pdf';
import HasP2 from './Assets/NormalNotePdfs/HeartandSouls.pdf';
import Hasmp31 from './Assets/mp3Normalouts/HeartAndSoulP1.mp3';
import Hasmp32 from './Assets/mp3Normalouts/HeartAndSoulP2.mp3';
import HasP2mscz from './Assets/MsczFiles/HAS.mscz';
import HasP1mscz from './Assets/MsczFiles/HAS1.mscz';

import Ser from './Assets/originWavs/Serious6B.wav';
import SerP1 from './Assets/NormalNotePdfs/Serious6B-Thesongoflove.pdf';
import SerP2 from './Assets/NormalNotePdfs/Serious6B.pdf';
import Sermp31 from './Assets/mp3Normalouts/P1Serious.mp3';
import Sermp32 from './Assets/mp3Normalouts/P2Serious.mp3';
import SerP2mscz from './Assets/MsczFiles/P2Serious.mscz';
import SerP1mscz from './Assets/MsczFiles/P1Serious.mscz';

import HHC from './Assets/originWavs/HearthomeCity.wav';
import HHCP1 from './Assets/NormalNotePdfs/HearthomeCity.pdf';
import HHCP2 from './Assets/NormalNotePdfs/Hearthomecityday.pdf';
import HHCmp31 from './Assets/mp3Normalouts/HHCP1.mp3';
import HHCmp32 from './Assets/mp3Normalouts/HHCP2.mp3';
import HHCP2mscz from './Assets/MsczFiles/HHCD2.mscz';
import HHCP1mscz from './Assets/MsczFiles/HHCD1.mscz';

class NormalNoteTests extends Component{
    render(){
		var str = "<- Basic Note Tests";
        return(
            <div>
            <section>
			<Nav />
		    </section>
			
			<div className='container'>
				<div className = 'row'>
				<div className = 'col centerT'>
					<div className='nav-link'>
                        <Link to='/BasicTests'>
							<a href>
                                {str}
							</a>
                        </Link>
					</div>
				</div>
					<div className = 'col centerT'>
						<div className='nav-link'>
                        <Link to='/HardTests'>
							<a href>
                                Hard Note Tests ->
							</a>
                        </Link>
						</div>
					</div>
                </div>
			</div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					My Heart Will Go On
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UCts9lFixyOqwhGCEbXErPwA'>
					PHianonize
				</a>
				<br/>
				<a href='https://www.youtube.com/watch?v=prbzAG1zKV8'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={Hrt}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={HrtP1mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Hrtmp31}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={HrtP2mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Hrtmp32}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={HrtP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HrtP1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={HrtP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HrtP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Heart and Soul
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UCs3A2cnH5RlculStplS3PcQ'>
						Easy Keys
				</a>
				<br/>
                <a href='https://www.youtube.com/watch?v=0x-wNJKzlAs'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={Has}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={HasP1mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Hasmp31}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={HasP2mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Hasmp32}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={HasP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HasP1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={HasP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HasP2}>to the PDF!</a></p>
                </object>
            </div>
			
            <div className='container set-bg'>
				<div className='text-center h3'>
					Serious 6B
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UCaHincMQCFZaYcjNWWHJUdQ'>
						Libera
				</a>
				<br/>
                <a href='https://www.youtube.com/watch?v=xQU396wKA3w'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={Ser}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={SerP1mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Sermp31}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={SerP2mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={Sermp32}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={SerP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={SerP1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={SerP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={SerP2}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Hearthome city
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UCCQ7wRJlwl7RFx-SPih9JoA'>
						Darren Ang
				</a>
				<br/>
                <a href='https://www.youtube.com/watch?v=q43XmJql58Q'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={HHC}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={HHCP1mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={HHCmp31}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={HHCP2mscz} download>
						mscz file used
					</a>
					<audio
						id='audio'
						controls
						src={HHCmp32}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={HHCP1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HHCP1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={HHCP2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={HHCP2}>to the PDF!</a></p>
                </object>
            </div>
			<Foot />
            </div>
        )
    }
}

export default NormalNoteTests;