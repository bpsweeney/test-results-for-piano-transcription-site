import React from 'react';
import {Routes, Route} from 'react-router-dom';
import Home from './Pages/Homepage';
import Basic from './Pages/BasicNoteTests';
import Hard from './Pages/HardNoteTests';
import Single from './Pages/SingleNoteTests';
import Normal from './Pages/NormalNoteTests';
import './App.css';

function App() {
  return (
    <div>
        <Routes>
          <Route exact path='/' element={<Home />} />
		  <Route exact path ='/BasicTests' element={<Basic />} />
		  <Route exact path='/SingleNotes' element={<Single />} />
		  <Route exact path='/NormalTests' element={<Normal />} />
		  <Route exact path='/HardTests' element={<Hard />} />
        </Routes>
    </div>
  );
}

export default App;
