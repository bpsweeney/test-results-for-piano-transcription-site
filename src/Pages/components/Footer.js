import React, { Component } from 'react';
import '../CSS/nav.css';
import '../CSS/style.css';

class Footer extends Component {
    render(){
        var u = "Check out the code to run the program on: "
		var v = "You can also look at the code for this website on this: "
        return(
            <div className="container">
                <footer className="center-text foot">
                    {u} 
                    <a href = "https://gitlab.com/bpsweeney/music-transcription">
                        Gitlab
                    </a>
					<br/>
					{v}
					<a href = "https://gitlab.com/bpsweeney/test-results-for-piano-transcription-site">
						Gitlab
					</a>
					<br/>
					I own none of the Music used on this site.  All assets belong to original composers and the creators of the piano sheets. 
					<br/>
					When sheets are created by others, we credit them in the resulting test. 
                </footer>
            </div>
        )
    }
}

export default Footer;