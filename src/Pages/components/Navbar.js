import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../CSS/nav.css';
import '../CSS/style.css';
import image from './piano.png';
import drop from './DropDowwn.js';

class Navbar extends Component {
    render(){
        return(
            <div className="container extra-container-bg">
                <nav className = 'navbar-holder nav-negative-box'>
                    <div className = 'nav-list leading-items'>
                    <div className = 'pad-right'>
                        <Link to='/'>
                            <a href>
                                <img height = '32' src = {image} alt='The thing'/>
                            </a>
                        </Link>
                    </div>
					{/* Same dropdown from a previous project*/}
					<span className="menuOpener" id="menu" onClick={drop}> For Test pages click me! </span>
					<ul className="userMenu" id="dropdown">
					
					<li className="item">
					<Link to='/SingleNotes'>
						<a href>
							Simple note tests
						</a>
					</Link>
					</li>
					
					<li className="item">
					<Link to='/BasicTests'>
						<a href>
							Basic song tests
						</a>
					</Link>
					</li>
					
					<li className="item">
					<Link to='/NormalTests'>
						<a href>
							Normal song tests
						</a>
					</Link>
					</li>
					
					<li className="item">
					<Link to='/HardTests'>
						<a href>
							Hard song tests
						</a>
					</Link>
					</li>
					
					</ul>
                    </div> 
					
                    <div className = 'nav-list trailing-items'>
                        <div className="nav-link">
                        <Link to='/'>
                            <a href>
                                Home
                            </a>
                        </Link>
                        </div>

                    </div>
                </nav> 
			</div>
        )
    }
}

export default Navbar;