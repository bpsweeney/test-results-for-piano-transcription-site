import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './CSS/Home.css';
import './CSS/style.css';
import './CSS/nav.css';
import Nav from './components/Navbar';
import Foot from './components/Footer';

import kirb1 from './Assets/HardPdfs/TheNewWorld.pdf'
import kirb from './Assets/HardPdfs/RunningThroughtheNewWorld.pdf';
import kirbP1out from './Assets/mp3Hardouts/kirb1.mp3';
import kirbP2out from './Assets/mp3Hardouts/kirb2.mp3';
import kirbori from './Assets/originWavs/Forgotten.wav';
import kirbP2mscz from './Assets/MsczFiles/kirb2.mscz';
import kirbP1mscz from './Assets/MsczFiles/kirb1.mscz';

import FE41 from './Assets/HardPdfs/FE41stgenend.pdf'
import FE4 from './Assets/HardPdfs/FE41stgenending.pdf';
import FE4P1out from './Assets/mp3Hardouts/FE4EndP1.mp3';
import FE4P2out from './Assets/mp3Hardouts/FE4EndP2.mp3';
import FE4ori from './Assets/originWavs/FE4End.wav';
import FE4P2mscz from './Assets/MsczFiles/FE4P2.mscz';
import FE4P1mscz from './Assets/MsczFiles/FE4P1.mscz';

import Sha1 from './Assets/HardPdfs/ShamaninthedarkP1.pdf'
import Sha2 from './Assets/HardPdfs/Shamaninthedark.pdf';
import ShaP1out from './Assets/mp3Hardouts/P1Sha.mp3';
import ShaP2out from './Assets/mp3Hardouts/SPP2.mp3';
import Shaori from './Assets/originWavs/SP140.wav';
import ShaP2mscz from './Assets/MsczFiles/ShaP2.mscz';
import ShaP1mscz from './Assets/MsczFiles/ShaP1.mscz';

class HardNoteTests extends Component{
    render(){
		var str = '<- Normal Note Tests';
        return(
            <div>
            <section>
			<Nav />
		    </section>
			
			<div className='container'>
				<div className = 'row'>
				<div className = 'col centerT'>
					<div className='nav-link'>
                        <Link to='/NormalTests'>
							<a href>
                                {str}
							</a>
                        </Link>
					</div>
				</div>
					<div className = 'col'>
					</div>
                </div>
			</div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Running Through the New World
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UC747XaKcl0_IXk-NyfI7tnw'>
						SolunaPiano
				</a>
				<br/>
                <a href='https://www.youtube.com/watch?v=c4Lwtzo3ms0'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={kirbori}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={kirbP1mscz} download>
						mscz file used at 300bpm
					</a>
					<audio
						id='audio'
						controls
						src={kirbP1out}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={kirbP2mscz} download>
						mscz file used at 150bpm
					</a>
					<audio
						id='audio'
						controls
						src={kirbP2out}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={kirb1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={kirb1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={kirb} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={kirb}>to the PDF!</a></p>
                </object>
            </div>
			
            <div className='container set-bg'>
				<div className='text-center h3'>
					FE4 Part 1 Map Melody
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.youtube.com/channel/UC5LQ2gkTjLdxDHsdktyoYlg'>
						garrrzzz
				</a>
				<br/>
                <a href='https://www.youtube.com/watch?v=pIsiVI5Nkbk'>
					Here is a link to the original song.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={FE4ori}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={FE4P1mscz} download>
						mscz file used at 180bpm
					</a>
					<audio
						id='audio'
						controls
						src={FE4P1out}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={FE4P2mscz} download>
						mscz file used at 180bpm
					</a>
					<audio
						id='audio'
						controls
						src={FE4P2out}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={FE41} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={FE41}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={FE4} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={FE4}>to the PDF!</a></p>
                </object>
            </div>
			
			<div className='container set-bg'>
				<div className='text-center h3'>
					Shaman in the Dark
				</div>
				<br />
				The creator of the original piano sheet: <a href='https://www.nicovideo.jp/user/18440220'>
						つかさ
				</a>
				<br/>
                <a href='https://www.nicovideo.jp/watch/sm20605107'>
					Here is a link to the original song on Niconico.
				</a>
				<br />
				<div className = 'row'>
				<div className = 'col'>
					Original wav file
					<audio
						id='audio'
						controls
						src={Shaori}
					></audio>
				</div>
				<div className = 'col'>
					P1 output:
					<a href={ShaP1mscz} download>
						mscz file used at 536bpm
					</a>
					<audio
						id='audio'
						controls
						src={ShaP1out}
					></audio>
				</div>
				<div className = 'col'>
					P2 output:
					<a href={ShaP2mscz} download>
						mscz file made with 8ths and 268bpm
					</a>
					<audio
						id='audio'
						controls
						src={ShaP2out}
					></audio>
				</div>
				</div>
				PDF output for program 1
				<object className="marg-top" title="PDF" data={Sha1} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Sha1}>to the PDF!</a></p>
                </object>
				PDF output for program 2
				<object className="marg-top" title="PDF" data={Sha2} type="application/pdf" width="100%" height="450">
                    <p>Here is a link to the pdf <a href={Sha2}>to the PDF!</a></p>
                </object>
            </div>
			
			<Foot />
            </div>
        )
    }
}

export default HardNoteTests;